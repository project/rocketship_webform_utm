<?php

namespace Drupal\rocketship_webform_utm\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;

/**
 * Provides a 'utm_tags' element.
 *
 * @WebformElement(
 *   id = "utm_tags",
 *   label = @Translation("UTM tags"),
 *   description = @Translation("Provides a form element to collect UTM tags from URL."),
 *   category = @Translation("Advanced elements"),
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 */
class UtmTags extends WebformCompositeBase {

  /**
   * List of available UTM tags.
   *
   * @var string[]
   */
  public static $utmTags = [
    'utm_source',
    'utm_medium',
    'utm_campaign',
    'utm_term',
    'utm_content',
  ];

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    $properties = [
      'title' => '',
    ];

    $composite_elements = $this->getCompositeElements();
    foreach ($composite_elements as $composite_key => $composite_element) {
      // Get #type, #title, and #option from composite elements.
      foreach ($composite_element as $composite_property_key => $composite_property_value) {
        if (in_array($composite_property_key, ['#type', '#title'])) {
          $property_key = str_replace('#', $composite_key . '__', $composite_property_key);
          if ($composite_property_value instanceof TranslatableMarkup) {
            $properties[$property_key] = (string) $composite_property_value;
          }
          else {
            $properties[$property_key] = $composite_property_value;
          }
        }
      }

      $properties[$composite_key . '__access'] = TRUE;
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['composite']['element']['#header']['visible'] = $this->t('Enabled');

    return $form;
  }

}
