<?php

namespace Drupal\rocketship_webform_utm\Element;

use Drupal\webform\Element\WebformCompositeBase;
use Drupal\rocketship_webform_utm\Plugin\WebformElement\UtmTags as UtmTagsWebformElement;

/**
 * Provides a 'utm_tags'.
 *
 * @FormElement("utm_tags")
 */
class UtmTags extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return parent::getInfo() + ['#wrapper_type' => 'container'];
  }

  /**
   * {@inheritdoc}
   */
  public static function getCompositeElements(array $element) {
    $elements = [
      '#attached' => [
        'library' => [
          'rocketship_webform_utm/populate_utm_tags',
        ],
        'drupalSettings' => [
          'rocketship_webform_utm' => [
            'tags' => UtmTagsWebformElement::$utmTags,
            'key' => $element['#webform_key'] ?? 'utm_tags',
          ],
        ],
      ],
    ];

    foreach (UtmTagsWebformElement::$utmTags as $tag) {
      $elements[$tag] = [
        '#type' => 'hidden',
        '#title' => $tag,
      ];
    }

    return $elements;
  }

}
