(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Populate UTM tags from query to the custom element.
   */
  Drupal.behaviors.populateUtmTags = {
    attach: function (context) {
      const settings = drupalSettings.rocketship_webform_utm || {};
      const utmTags = settings.tags || [];

      const rawQuery = window.location.search.split('?')[1] || '';
      const formattedQuery = {};

      // Skip the process when no query available.
      if (!rawQuery.length) {
        return;
      }

      const rawQueryParts = rawQuery.split('&');
      for (let i = 0; i < rawQueryParts.length; i++) {
        const parts = rawQueryParts[i].split('=');
        formattedQuery[parts[0]] = parts[1];
      }

      for (let i = 0; i < utmTags.length; i++) {
        const utmTagName = utmTags[i];

        if (typeof formattedQuery[utmTagName] !== 'undefined') {
          const utmTagValue = formattedQuery[utmTagName];

          $(context)
            .find(`input[name="${settings.key}[${utmTagName}]"]`)
            .val(utmTagValue);
        }
      }
    }
  };

})(jQuery, Drupal, drupalSettings);
